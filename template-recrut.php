<?php
/* Template Name: Recrut */
global $web;
if (is_user_logged_in()){
    if (current_user_is('candidat')){
        wp_redirect(path($web['page']['candidat']['slug']));
    }
}else{
    wp_redirect(path($web['page']['404']['slug']));
}
get_header(); ?>

<section id="background">
    <div class="wrap">
        <div id="recruteurs">
            <h1>Vous êtes recruteurs</h1>
        </div>

        <table id="cv-table">
            <thead>
            <tr id="titreRecrut">
                <th>Prénom</th>
                <th>Emploi recherché</th>
                <th>C.V. envoyé le </th>
                <th>Consulter</th>
                <th>Télécharger</th>
            </tr>
            </thead>
            <tbody id="cv-table-body"></tbody>
        </table>
        <div class="list_cv">
            <ul class="all_list">

            </ul>

        </div>
        <div class="pagination" id="pagination-controls"></div>


    </div>
</section>

<?php get_footer();

