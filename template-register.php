<?php
/* Template Name: Register */
global $web;
if (is_user_logged_in()){
    wp_redirect(path($web['page']['homepage']['slug']));
}
global $web;
$metaRegister=get_post_meta($web['page']['register']['id']);
get_header();
$nb1= rand(0,5);
$nb2= rand(0,5);
$nb3= $nb1+ $nb2;
$nb4= rand(0,10);
$nb5= rand(0,10);
$nb6= rand(0,10);
$total=[
    $nb6=>$nb6,
    $nb5=>$nb5,
    $nb4=>$nb4,
    $nb3=>$nb3
];
//if (is_user_logged_in()){
//    header('Location: header.php');
//}
?>

<section id="register">
    <div class="background_register"><?php echo imgById($metaRegister, 'back1');  ?></div>
    <div class="background_register2"><?php echo imgById($metaRegister, 'back2');  ?></div>
    <div class="wrap2">
        <form class="form_register">
            <h1><?php echo getMetaText($metaRegister, 'titre_register'); ?></h1>
            <div class="genre_register">
                <div class="femme genre">
                    <label for="femme"><?php echo getMetaText($metaRegister, 'madame'); ?></label>
                    <input type="radio" name="genre" value="Mme." id="femme" checked>
                </div>
                <div class="homme_register genre">
                    <label for="homme"><?php echo getMetaText($metaRegister, 'monsieur'); ?></label>
                    <input type="radio" name="genre" value="Mr." id="homme">
                </div>
            </div>
            <div class="register_name">
                <div class="register_nom name">
                    <label for="nom"><?php echo getMetaText($metaRegister, 'label_nom'); ?></label>
                    <input type="text" name="nom" class="nom_register" placeholder="<?php echo getMetaText($metaRegister, 'placeholder_nom'); ?>">
                    <span class="error nom_error"></span>
                </div>
                <div class="register_prenom name">
                    <label for="prenom"><?php echo getMetaText($metaRegister, 'label_prenom'); ?></label>
                    <input type="text" name="prenom" class="prenom_register" placeholder="<?php echo getMetaText($metaRegister, 'placeholder_prenom'); ?>">
                    <span class="error prenom_error"></span>
                </div>
            </div>
            <div class="register_email">
                <label for="email"><?php echo getMetaText($metaRegister, 'label_email'); ?></label>
                <input type="email" name="email" class="email_register" placeholder="<?php echo getMetaText($metaRegister, 'placeholder_email'); ?>">
                <span class="error email_error"></span>
            </div>
            <div class="date_de_naissance">
                <div class="date_display">
                    <label for="date"><?php echo getMetaText($metaRegister, 'label_naissance'); ?></label>
                    <input type="date" name="date" id="date" class="date_register">
                </div>
                <div class="error date_error"></div>
            </div>
            <div class="voiture">
                <h4><?php echo getMetaText($metaRegister, 'label_voiture'); ?></h4>
                <div class="response_voiture">
                    <div class="oui_register">
                        <input type="radio" id="oui" name="vehicule">
                        <label for="oui"><?php echo getMetaText($metaRegister, 'reponse_voiture_1'); ?></label>
                    </div>
                    <div class="non_register">
                        <input type="radio" id="non" name="vehicule" checked>
                        <label for="non"><?php echo getMetaText($metaRegister, 'reponse_voiture_2'); ?></label>
                    </div>
                </div>
            </div>
            <div class="all_password_register">
                <div class="password1">
                    <label for="password1"><?php echo getMetaText($metaRegister, 'label_password'); ?></label>
                    <input type="password" id="password1" name="password1">
                </div>
                <div class="password2">
                    <label for="password2"><?php echo getMetaText($metaRegister, 'label_password_2'); ?></label>
                    <input type="password" id="password2" name="password2">
                    <span class="error pas2"></span>
                </div>
            </div>
            <div class="verif_human">
                <label for="calcul">Vérification : <span class="nb1"><?php echo $nb1 ?></span> + <span class="nb2"><?php echo $nb2 ?></span> ?</label>
<!--                <input type="">-->
                <div class="form_calcul form_display">
                    <select name="calcul" id="total">
                        <option value="">_sélectionnez une réponse_</option>
                        <?php foreach ($total as $key => $value) { ?>
                            <option value="<?php echo $key; ?>"<?php
                            if(!empty($_POST['genre']) && $_POST['genre'] === $key) {
                                echo ' selected';
                            }
                            ?>><?php echo $value; ?></option>
                        <?php } ?>
                    </select>
                    <span class="error calcul"><?php if(!empty($errors['genre'])) { echo $errors['genre']; } ?></span>
                </div>
            </div>
            <input type="submit" id="submit" class="submit" value="<?php echo getMetaText($metaRegister, 'envoyer'); ?>">
        </form>
    </div>
</section>

<?php
get_footer();

