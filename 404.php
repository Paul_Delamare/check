<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package check
 */
global $web;
get_header();
?>

    <main id="primary" class="site-main">

        <section class="error-404 not-found">
            <header class="page-header">
                <h1 class="page-title" style="text-align: center"><?php esc_html_e( 'Oops! Cette page est introuvable.', 'CV-CHECK' ); ?></h1>
            </header>
            <img src="<?= asset('img/404/clown.jpg'); ?>" alt="pizzanet header" style="width: 15%; display: flex; margin: auto">
        </section>
        <div id="lien404">
            <a href="<?php echo path($web['page']['homepage']['slug']); ?>">JE SOUHATE RETOURNER SUR CV-CHECK</a>
        </div>
    </main>

<?php
get_footer();
