<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package check
 */
global $web;
global $metaHome;
?>
<div id="return">
    <a href="#masthead">
        <i class="fa-solid fa-circle-arrow-up"></i>
    </a>
</div>
<footer id="footer">
    <img class="backfooter" src="<?= asset('img/footer/backFooter.png'); ?>" alt="back footer">
    <div id="pages">
        <div class="pages1">
            <h5>Nos pages principale</h5>
            <a href="<?php echo path($web['page']['cv']['slug']); ?>">Créer mon CV</a>
            <a href="<?php echo path($web['page']['homepage']['slug']); ?>#carte">Nous contacter</a>
            <?php if (!is_user_logged_in()){
                ?>
                <a href="<?php echo path($web['page']['register']['slug']); ?>">S'enregistrer</a>
                <a href="">Se connecter</a>
                <?php
            }  ?>

        </div>
        <div class="pages2">
            <h5>Nos pages complémentaires</h5>
            <a href="<?php echo path($web['page']['mentions']['slug']); ?>">Mention légales</a>
            <a href="<?php echo path($web['page']['cgu']['slug']); ?>">Conditions générales d'utilisation</a>
        </div>
        <div class="pages3">
            <h5>Nos réseaux sociaux</h5>
            <div id="svgFooter">
                <a href="<?php echo getMetaText($metaHome, 'lien_reseau_1');  ?>" target="_blank">
                    <?php echo getMetaText($metaHome, 'logo_reseau_1');  ?>
                </a>
                <a href="<?php echo getMetaText($metaHome, 'lien_reseau_2');  ?>" target="_blank">
                    <?php echo getMetaText($metaHome, 'logo_reseau_2');  ?>
                </a>
                <a href="<?php echo getMetaText($metaHome, 'lien_reseau_3');  ?>" target="_blank">
                    <?php echo getMetaText($metaHome, 'logo_reseau_3');  ?>
                </a>
            </div>
        </div>
    </div>
    <div id="logoFooter">
        <a href="index.php">
            <img src="<?= asset('img/footer/CV-CHECK.png'); ?>" alt="logo footer">
        </a>
        <p>© CVCHECK. 2023. All rights reserved.</p>
    </div>
</footer>

<?php wp_footer(); ?>

</body>
</html>
