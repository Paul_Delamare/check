<?php
/* Template Name: formatCV */


get_header();
global $wpdb;
$current_user_id = get_current_user_id();

// Retrieve the data from the "cv" table for the current user
$cv_data = $wpdb->get_row(query: "SELECT * FROM {$wpdb->prefix}cv WHERE ID_user = $current_user_id");

// Retrieve the data from the "hobbies" table for the current user
$hobbies_data = $wpdb->get_results(query: "SELECT * FROM {$wpdb->prefix}hobbies WHERE Id_CV = $cv_data->Id");
debug($hobbies_data);


// Retrieve the data from the "experiences" table for the current user
$experiences_data = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}experiences WHERE ID_CV = $cv_data->Id");
//debug($experiences_data);
// Retrieve the data from the "diplome" table for the current user
$diplome_data = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}diplome WHERE Id_cv = $cv_data->Id");
debug($diplome_data);
?>
<section id="cv">
   <div class="wrap">
       <div class="nameMetier">
           <h1><?php echo $cv_data->name ?></h1>
           <h2 style="color: #0c0c0c"><?php echo $cv_data->demande ?></h2>
           <div class="underline"></div>
       </div>

       <div  class="profile_experience">
           <div>
               <h2>profile</h2>
               <p><?php echo $cv_data->Distinction_title?></p>
           </div>
           <div>
               <h2>experiences</h2>
               <p></p>
           </div>
       </div>
       <div class="coordonnées">
           <h2>Coordonnées</h2>
           <p><?php echo $cv_data->Adress ?></p>
       </div>
       <div class="competences_formations">
           <div class="compentences">
               <h2>competences</h2>
               <p></p>
           </div>
           <div class="formations">
               <h2>formations</h2>
               <p><?php echo $diplome_data->content ?></p>
           </div>
       </div>
       <div class="loisirs">
           <h2>
               loisirs
           </h2>
           <p><?php echo $hobbies_data->Date ?></p>
       </div>
   </div>
</section>
<?php
get_footer();
