<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package check
 */
global $web,$metaHome;
$metaHome= get_post_meta($web['page']['homepage']['id']);
if (is_user_logged_in()){
    if (current_user_is('candidat')){
        show_admin_bar(false);
    }
}

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<div id="page" class="site">

<header id="masthead" class="site-header">
    <div class="wrap">
        <div class="logo">
            <a href="<?php echo path($web['page']['homepage']['slug']); ?>">
                <?php echo imgById($metaHome, 'logo', 'img_logo'); ?>
            </a>
<!--             --><?php //$img = asset('img/cv-check.png') ; ?>
<!--            <img src="--><?php //= $img; ?><!--" alt="">-->
        </div>
        <nav>
            <ul>
                <div class="responsive">

                    <?php if (is_user_logged_in()){
                    if (current_user_is('administrator')){
                        ?>
                        <li><a class="connecte" href="<?php echo path($web['page']['recrut']['slug']); ?>"><?php echo $metaHome['voir_cv'][0]; ?></a></li>
                        <?php
                    }else{
                        ?>
                        <li><a class="<?php if (is_user_logged_in()){ echo 'connecte'; } ?>" href="<?php echo path($web['page']['cv']['slug']); ?>"><?php echo $metaHome['cv'][0]; ?></a></li>
                        <?php
                    }  }else{
                        ?>
                        <li><a class="<?php if (is_user_logged_in()){ echo 'connecte'; } ?>" href="<?php echo path($web['page']['cv']['slug']); ?>"><?php echo $metaHome['cv'][0]; ?></a></li>
                        <?php
                    }  ?>
                    <?php if (!is_user_logged_in()){
                        ?>
                        <li><a class="<?php if (is_user_logged_in()){ echo 'connecte'; } ?>" href="<?php echo path($web['page']['homepage']['slug']); ?>#carte"><?php echo $metaHome['contact'][0]; ?></a></li>
                        <?php
                    }elseif ( current_user_is('candidat')){
                        ?>
                        <li><a class="<?php if (is_user_logged_in()){ echo 'connecte'; } ?>" href="<?php echo path($web['page']['candidat']['slug']); ?>">Historique</a></li>
                        <?php
                    }   ?>
                    <?php if (!is_user_logged_in()){
                        ?>
                        <li class="login"><a href=""><?php echo $metaHome['connexion'][0];; ?></a></li>
                        <li class="register"><a href="<?php echo path($web['page']['register']['slug']); ?>"><?php echo $metaHome['register'][0]; ?></a></li>
                        <?php
                    }else{
                        ?>
                        <li class="register"><a class="connecte" href="<?php echo wp_logout_url( path($web['page']['homepage']['slug']) ); ?>">Se déconnecter</a></li>
                        <?php
                    }   ?>
                </div>
                <div class="burger">
                    <li class="open_burger"><i class="fa-solid fa-bars"></i></li>
                </div>
            </ul>
        </nav>
        <div class="burger_menu">
            <h3 class="close_burger"><i class="fa-solid fa-x"></i></h3>
            <ul>
                <?php if (!is_user_logged_in()){
                    ?>
                    <li class="register"><a href="<?php echo path($web['page']['register']['slug']); ?>"><?php echo $metaHome['register'][0]; ?></a></li>
                    <li class="login"><a class="loginBtn" href=""><?php echo $metaHome['connexion'][0]; ?></a></li>
                    <?php
                }  ?>
                <li><a href=""><?php echo $metaHome['contact'][0];; ?></a></li>
                <li><a href=""><?php echo $metaHome['cv'][0];; ?></a></li>
                <?php if (is_user_logged_in()){
                    ?>
                    <li class=""><a href="<?php echo wp_logout_url( get_permalink() ); ?>">Se déconnecter</a></li>
                    <?php
                }  ?>
            </ul>
        </div>
    </div>
</header>
<section id="modal_login">
    <div id="myModal" class="modal">
        <div class="modal-content">
            <div class="background_login">
                <?php echo imgById($metaHome, 'background_login'); ?>
            </div>
            <div class="background_login2">
                <?php echo imgById($metaHome, 'background_login_2'); ?>
            </div>
            <div class="modal-header">
                <span class="close">&times;</span>
            </div>
            <form class="connexion">
                <h1><b class="titre1"><?php echo $metaHome['titre_login_1'][0]?></b><?php echo $metaHome['titre_login_2'][0]; ?></h1>
                <div class="email_login input_login">
                    <input class="email" type="text" id="email" name="email" placeholder="<?php echo getMetaText($metaHome, 'placeholder_email'); ?>">
                </div>
                <div class="password_login input_login">
                    <input class="password" type="password" id="password" name="password" placeholder="<?php echo getMetaText($metaHome, 'placeholder_password'); ?>">
                    <span class="error error_login"></span>
                </div>
                <div class="checkbox_login">
                    <label for="checkbox"><?php echo getMetaText($metaHome, 'remember'); ?></label>
                    <input class="checkbox" type="checkbox" id="checkbox" name="checkbox">
                </div>
                <div class="input_submit">
                    <input class="submit" type="submit" id="submit" name="submit" value="<?php echo getMetaText($metaHome, 'connexion_input'); ?>">
                </div>
                <div class="oublie_password">
                    <a href=""><?php echo getMetaText($metaHome, 'password_oublie'); ?></a>
                </div>
            </form>
        </div>
    </div>
</section>