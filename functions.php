<?php

require __DIR__ .'\vendor\autoload.php';

require get_template_directory().'/inc/func.php';
require get_template_directory().'/inc/general.php';
require get_template_directory().'/inc/image.php';
require get_template_directory().'/inc/parameters.php';
require get_template_directory().'/inc/parameters.php';

//CUSTOM
require get_template_directory().'/inc/custom/custom-partner.php';


//AJAX
require get_template_directory().'/inc/admin/ajax-requet.php';
require get_template_directory().'/inc/ajax/ajax-login.php';
require get_template_directory().'/inc/ajax/ajax-register.php';

require get_template_directory().'/inc/ajax/ajax-cv.php';
require get_template_directory().'/inc/ajax/ajax-recrut.php';

require get_template_directory() . '/inc/extra/template-tags.php';
require get_template_directory() . '/inc/extra/template-functions.php';
