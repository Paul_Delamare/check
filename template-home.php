<?php
/* Template Name: HomePage */
global $web,$metaHome;
if (is_user_logged_in()){
    if (current_user_is('administrator')){
        wp_redirect(path($web['page']['recrut']['slug']));
    }elseif (current_user_is('candidat')){
        wp_redirect(path($web['page']['candidat']['slug']));
    }
}
get_header();
?>

<section id="intro">
    <div class="wrap">
        <div class="flex">
            <div class="texte_gauche">
                <h1><?php echo getMetaText($metaHome, 'titre_intro_1'); ?><br>
                    <b><?php echo getMetaText($metaHome, 'titre_intro_2'); ?></b></h1>
                <p><?php echo getMetaText($metaHome, 'text_intro_gris'); ?><span class="blue_home"> <?php echo getMetaText($metaHome, 'text_intro_bleu'); ?></span><br><span class="anim_home" is="type-async" id="type-text">...</span><span class="blinking-cursor">_</span></p>
                <a href="<?php echo path($web['page']['cv']['slug']); ?>"><?php echo getMetaText($metaHome, 'bouton_intro'); ?></a>
            </div>
            <div class="img_droite">
                <?php $img = asset('img/imagehome.png'); ?>
                <img src="<?= $img; ?>" alt="">
            </div>
        </div>
    </div>
</section>

<section id="presentation" class="wrap">
    <div class="flex">
        <div class="card">
            <svg width="102" height="126" viewBox="0 0 102 126" fill="none" xmlns="http://www.w3.org/2000/svg">
                <ellipse cx="51" cy="47.8343" rx="51" ry="47.8343" fill="#FCECEB"/>
                <path d="M50.7187 39.9163C52.0659 45.9836 54.5349 77.7123 49.6378 78.8297C44.7407 79.9471 34.3319 50.03 32.9848 43.9628C31.6376 37.8955 35.7763 39.5745 40.6734 38.4571C45.5705 37.3398 49.3715 33.8491 50.7187 39.9163Z" fill="#C80E0E"/>
                <path d="M20.2587 65.2733C24.9582 67.0806 47.5045 79.5252 46.0881 83.2236C44.6716 86.922 19.8286 80.4737 15.1292 78.6664C10.4297 76.8591 13.4005 74.8959 14.8169 71.1975C16.2334 67.4991 15.5592 63.466 20.2587 65.2733Z" fill="#C80E0E"/>
                <path d="M29.2947 98.6123C31.4068 95.6558 45.3098 82.99 48.9119 85.5884C52.514 88.1867 44.4511 105.065 42.339 108.022C40.2269 110.978 38.4596 107.86 34.8575 105.261C31.2555 102.663 27.1826 101.569 29.2947 98.6123Z" fill="#C80E0E"/>
                <path d="M69.9646 111.903C66.1034 108.538 49.5206 88.0376 52.8673 84.1549C56.2141 80.2723 78.2231 94.4778 82.0843 97.843C85.9454 101.208 81.9068 102.665 78.56 106.548C75.2133 110.431 73.8257 115.269 69.9646 111.903Z" fill="#C80E0E"/>
                <path d="M84.8712 65.1305C81.3474 68.877 60.4264 84.9335 56.9737 81.6479C53.521 78.3623 68.844 56.9787 72.3678 53.2322C75.8915 49.4857 76.9685 53.4324 80.4212 56.718C83.874 60.0036 88.3949 61.384 84.8712 65.1305Z" fill="#C80E0E"/>
            </svg>
            <h2><?php echo getMetaText($metaHome, 'titre_pres_1'); ?></h2>
            <p>
                <?php echo getMetaText($metaHome, 'contenu_pres_1'); ?>
            </p>
        </div>
        <div class="card">
            <svg width="104" height="105" viewBox="0 0 104 105" fill="none" xmlns="http://www.w3.org/2000/svg">
                <ellipse cx="52" cy="52.5" rx="52" ry="52.5" fill="#EBF2FF"/>
                <path d="M47.2783 8C51.3536 21.7837 49.2344 29.2927 44.6701 35.2587C39.6982 41.842 31.9551 46.7794 26.4942 56.4486C19.2401 69.3065 18.0175 97.5938 44.181 105C33.1777 97.6967 30.814 76.5069 42.7139 63.2375C39.6167 76.0954 45.3221 84.3245 52.4132 81.3415C59.3412 78.3584 63.9056 84.6331 63.7426 91.8335C63.6611 96.7709 62.1124 100.988 58.1186 103.354C75.1535 99.5482 82 81.7529 82 68.175C82 50.2768 69.3665 47.8081 75.724 32.79C68.1439 33.6129 65.5357 39.8876 66.2693 50.1739C66.7583 56.9629 61.1344 61.5917 56.9775 58.5058C53.6358 55.9343 53.7173 50.9968 56.6515 47.2937C62.9275 39.4761 65.3727 21.4751 47.2783 8Z" fill="#377DFF"/>
            </svg>
            <h2><?php echo getMetaText($metaHome, 'titre_pres_2'); ?></h2>
            <p>
                <?php echo getMetaText($metaHome, 'contenu_pres_2'); ?>
            </p>
        </div>
        <div class="card">
            <svg width="122" height="128" viewBox="0 0 122 128" fill="none" xmlns="http://www.w3.org/2000/svg">
                <ellipse cx="61" cy="60" rx="61" ry="60" fill="#E5F9F6"/>
                <path d="M52.0648 97.2938C49.7581 91.8818 52.4023 85.6442 57.9346 83.3876C63.4669 81.1311 69.8431 83.7179 72.1498 89.1299C74.4565 94.5419 71.8122 100.78 66.28 103.036C60.7477 105.293 54.3715 102.706 52.0648 97.2938ZM96.1356 100.89L91.0346 98.8349C91.7848 95.184 91.8035 91.3498 91.0346 87.5705L96.1168 85.4974C97.3546 85.0021 97.9547 83.6078 97.4296 82.3786L94.8979 76.3979C94.3915 75.1871 92.9662 74.6 91.7098 75.1137L86.6088 77.1868C84.4146 73.9762 81.6203 71.2794 78.451 69.2246L80.5514 64.2346C81.0577 63.0237 80.4764 61.6295 79.2387 61.1341L73.1438 58.6391C71.906 58.1438 70.4807 58.7125 69.9744 59.9233L67.874 64.9134C65.6236 64.4731 63.2981 64.2896 60.9727 64.3813C60.5039 67.7937 57.7658 70.4905 54.3152 70.9859C54.2777 71.0592 54.2589 71.1143 54.2214 71.1877C55.1966 72.4902 55.7405 74.068 55.7405 75.7374C55.7405 76.4346 55.6467 77.095 55.4592 77.7555C55.4967 77.7371 55.5154 77.7188 55.5529 77.7188C64.2921 74.1597 74.3627 78.2325 78.0009 86.8C81.6391 95.3491 77.4758 105.201 68.7179 108.76C59.9788 112.319 49.9081 108.246 46.2699 99.6788C44.282 94.9639 44.6384 89.8821 46.8325 85.6625C44.7321 86.0295 42.5005 85.5892 40.7189 84.2866C40.6439 84.3233 40.5876 84.3416 40.5126 84.36C39.9312 88.0475 36.6681 90.8728 32.7486 90.8728C32.5236 93.5146 32.6548 96.2114 33.2175 98.9082L28.1352 100.981C26.8975 101.477 26.2974 102.871 26.8225 104.1L29.3542 110.081C29.8606 111.292 31.2858 111.879 32.5423 111.365L37.6433 109.292C39.8375 112.503 42.6317 115.199 45.8011 117.254L43.7007 122.244C43.1943 123.455 43.7757 124.849 45.0134 125.345L51.1458 127.821C52.3836 128.317 53.8088 127.748 54.3152 126.537L56.4156 121.547C60.1475 122.281 64.067 122.299 67.949 121.547L70.0682 126.519C70.5745 127.73 71.9998 128.317 73.2563 127.803L79.3699 125.326C80.6077 124.831 81.2078 123.437 80.6827 122.207L78.5635 117.236C81.8454 115.089 84.6022 112.356 86.7026 109.255L91.8035 111.31C93.0413 111.805 94.4665 111.237 94.9729 110.026L97.5046 104.027C97.9734 102.779 97.3733 101.403 96.1356 100.89ZM12.2135 65.1702L9.04414 65.1519C7.91892 65.1519 7 64.2529 7 63.1338L7.01875 59.7582C7.01875 58.6574 7.93768 57.7585 9.08164 57.7585L12.251 57.7768C12.7386 55.3919 13.7138 53.0986 15.1578 51.0256L12.9449 48.8424C12.1572 48.0535 12.1572 46.7877 12.9636 46.0171L15.4016 43.6505C16.208 42.88 17.502 42.88 18.2896 43.6689L20.5213 45.8704C22.6592 44.4761 25.0222 43.5588 27.4601 43.1001L27.4789 39.9997C27.4789 38.8989 28.3978 38 29.5418 38L32.9924 38.0183C34.1176 38.0183 35.0365 38.9173 35.0365 40.0364L35.0178 43.1368C37.4558 43.6138 39.7999 44.5678 41.9191 45.9804L44.1695 43.7973C44.9759 43.0268 46.2699 43.0268 47.0576 43.8156L49.4768 46.2006C50.2644 46.9895 50.2644 48.2553 49.458 49.0259L47.2076 51.209C48.6329 53.3004 49.5705 55.612 50.0394 57.997L53.2087 58.0153C54.3339 58.0153 55.2529 58.9143 55.2529 60.0334L55.2341 63.409C55.2341 64.5098 54.3152 65.4087 53.1712 65.4087L50.0019 65.3904C49.5143 67.7753 48.5391 70.0686 47.0951 72.1416L49.3267 74.3431C50.1144 75.132 50.1144 76.3979 49.308 77.1684L46.87 79.535C46.0636 80.3055 44.7696 80.3055 43.982 79.5167L41.7503 77.3152C39.6124 78.7095 37.2495 79.6267 34.8115 80.0854L34.7928 83.1858C34.7928 84.2866 33.8738 85.1855 32.7299 85.1855L29.2792 85.1672C28.154 85.1672 27.2351 84.2683 27.2351 83.1492L27.2538 80.0487C24.8159 79.5717 22.4717 78.6177 20.3525 77.2051L18.1021 79.3883C17.2957 80.1588 16.0017 80.1588 15.2141 79.3699L12.7948 76.9849C12.0072 76.1961 12.0072 74.9302 12.8136 74.1597L15.064 71.9765C13.62 69.8668 12.6823 67.5552 12.2135 65.1702ZM24.6096 67.8854C28.1727 71.4078 33.9676 71.4445 37.5683 67.9588C41.169 64.4731 41.2065 58.8042 37.6433 55.2818C34.0801 51.7594 28.2853 51.7227 24.6846 55.2084C21.0652 58.6941 21.0464 64.363 24.6096 67.8854Z" fill="#00C9A7"/>
            </svg>
            <h2><?php echo getMetaText($metaHome, 'titre_pres_3'); ?></h2>
            <p>
                <?php echo getMetaText($metaHome, 'contenu_pres_3'); ?>
            </p>
        </div>
    </div>
</section>

<section id="video">
    <div class="wrap2">
        <div class="img_fond">
            <?php $img_fond = asset('img/Ellipse4.png'); ?>
            <img src="<?= $img_fond; ?>" alt="">
        </div>
        <div class="img_fond2">
            <?php $img_fond2 = asset('img/Ellipse3.png'); ?>
            <img src="<?= $img_fond2; ?>" alt="">
        </div>
        <div class="video_pres">
            <h2><?php echo getMetaText($metaHome, 'titre_video'); ?></h2>
            <iframe src="https://www.youtube.com/embed/<?php echo getMetaText($metaHome, 'url_video'); ?>" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
        </div>
    </div>
</section>
<?php

$args = [
    'post_type' => 'partner',
    'posts_per_page'=>-1,
    'orderby'=>'date',
    'order'=> 'ASC',
];

$the_query = new WP_Query($args);
?>
<section id="partners">
    <div class="wrap">
        <div class="titre">
            <h1><?php echo getMetaText($metaHome, 'titre_partenaire'); ?></h1>
        </div>
        <div class="all_partner">
            <div class="flex_partner">
                <div class="flexslider carousel">
                    <ul class="slides">
                       <?php if ($the_query->have_posts()) {
                            while ($the_query->have_posts()) {
                                $the_query->the_post();?>
                                <li>
                                    <img src="<?= get_the_post_thumbnail_url(get_the_ID(), 'img_partner') ?>" alt="<?php echo get_the_title() ?>">
                                </li>
                        <?php }
                       }  ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="quisommesnous">
    <div class="wrap">
        <div class="titre">
            <h1><?php echo getMetaText($metaHome, 'titre_nous'); ?></h1>
        </div>
        <div class="flex">
            <div class="card">
                <?php $img9 = asset('img/Vector.png'); ?>
                <img src="<?= $img9;?>" alt="">
                <div class="infos_card">
                    <h4>Kévin</h4>
                    <p>Chef de projet</p>
                    <p>Développeur full-stack</p>
                </div>
            </div>
            <div class="card">
                <img src="<?= $img9;?>" alt="">
                <div class="infos_card">
                    <h4>Romain</h4>
                    <p>Développeur Full-stack</p>
                </div>
            </div>
            <div class="card">
                <img src="<?= $img9;?>" alt="">
                <div class="infos_card">
                    <h4>Lendoly</h4>
                    <p>Développeur Full-stack</p>
                </div>
            </div>
            <div class="card">
                <img src="<?= $img9;?>" alt="">
                <div class="infos_card">
                    <h4>Paul</h4>
                    <p>Développeur Full-stack</p>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="carte">
    <h1><?php echo getMetaText($metaHome, 'titre_carte'); ?></h1>
    <div class="wrap">
        <div class="img_fond3">
            <?php $img_fond3 = asset('img/Ellipse1.png'); ?>
            <img src="<?= $img_fond3; ?>" alt="">
        </div>
        <div class="flex">
            <div class="img_carte">
                <div id="mapid" "></div>
            </div>
            <div class="card">
                <?php $img11 = asset('img/Vector2.png'); ?>
                <div class="adresse">
                    <i class="fa-solid fa-location-dot"></i>
                    <p><?php echo getMetaText($metaHome, 'adresse'); ?></p>
                    <p><?php echo getMetaText($metaHome, 'ville'); ?></p>
                </div>
                <div class="contact">
                    <p><?php echo getMetaText($metaHome, 'telephone'); ?></p>
                    <p><?php echo getMetaText($metaHome, 'email'); ?></p>
                </div>
            </div>
        </div>
    </div>
</section>


<section id="poste">
    <div class="wrap2">
        <div class="img_fond4">
            <?php $img_fond4 = asset('img/Ellipse2.png'); ?>
            <img src="<?= $img_fond4; ?>" alt="">
        </div>
        <div class="titre">
            <h1><?php echo getMetaText($metaHome, 'titre_posts_1'); ?></h1>
            <p><?php echo getMetaText($metaHome, 'titre_posts_2'); ?></p>
        </div>
        <div class="flex">
            <div class="card">
                <p>Agent-immobilier</p>
            </div>
            <div class="card">
                <p class="blue">Chargé de communication</p>
            </div>
            <div class="card">
                <p>Designer</p>
            </div>
            <div class="card">
                <p>Web-Marketing</p>
            </div>
            <div class="card">
                <p class="blue">Secrétaire</p>
            </div>
            <div class="card">
                <p>Développeur/se</p>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    var map = L.map('mapid').setView([<?php echo getMetaText($metaHome, 'latitude_adresse'); ?>, <?php echo getMetaText($metaHome, 'longitude_adresse'); ?>], <?php echo getMetaText($metaHome, 'zoom_carte'); ?>);
    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '© <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(map);
    var marker = L.marker([<?php echo getMetaText($metaHome, 'latitude_adresse'); ?>, <?php echo getMetaText($metaHome, 'longitude_adresse'); ?>]).addTo(map);

</script>
<?php
get_footer();

//<!--                <div class="img_partner">-->
//<!--                    --><?php //$img5 = asset('img/edf.png'); ?>
<!--                    <img src="--><?php //= $img5; ?><!--" alt="">-->
<!--                </div>-->
<!--                <div class="img_partner">-->
<!--                    --><?php //$img6 = asset('img/cdiscount.png'); ?>
<!--                    <img src="--><?php //= $img6; ?><!--" alt="">-->
<!--                </div>-->
<!--                <div class="img_partner">-->
<!--                    --><?php //$img7 = asset('img/ldlc.png'); ?>
<!--                    <img src="--><?php //= $img7; ?><!--" alt="">-->
<!--                </div>-->
<!--                <div class="img_partner">-->
<!--                    --><?php //$img8 = asset('img/sos.png'); ?>
<!--                    <img src="--><?php //= $img8; ?><!--" alt="">-->
<!--                </div>-->
