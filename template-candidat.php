<?php
/* Template Name: Candidat */
global $web;
if (is_user_logged_in()){
    if (current_user_is('administrator')){
        wp_redirect(path($web['page']['historique']['slug']));
    }
}else{
    wp_redirect(path($web['page']['404']['slug']));
}
get_header();
?>

<section id="candidat">
    <div class="wrap">
        <h2>Tous vos C.V. :</h2>
        <div class="candidat">
            <div class="table-responsive">
                <ul class="historique table-bordered">

                </ul>
            </div>
        </div>
    </div>
</section>



<?php
get_footer();