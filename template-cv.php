<?php
/* Template Name: Cv */

global $web;
if (is_user_logged_in()){
    if (current_user_is('administrator')){
        wp_redirect(path($web['page']['recrut']['slug']));
    }
}
global $wpdb;
$errors = array();
get_header();
?>
<section id="formulaire-cv">
    <div class="bg-creaCv">
        <img src="<?php echo asset('/img/bgimg.png')  ?>">
    </div>
    <img class="img2" src="<?php echo asset('/img/bgimg.png')  ?>">
    <div class="wrap2">
        <?php if (!is_user_logged_in()) {
            ?>
            <h2 class="attention">Attention ! Vous n'êtes pas connecté. Vous ne pourrez pas consulter ou modifier votre c.v. une fois envoyé.</h2>
            <?php
        }  ?>
         <form id="form-cv">
             <div>
                 <label for="Nom&prenom">Nom & prénom</label>

                 <input type="text" id="nom" name="nom">

                 <span class="error" id="error_nom"></span>
             </div>
             <div>
                 <label for="">Email</label>
                 <input type="email" class="email_cv" id="email" name="email">
                 <p class="error" id="error_email"></p>
             </div>
             <div>
                 <label for="">Téléphone</label>
                 <input type="tel" id="tel" name="tel" pattern="[0-9]{2}[0-9]{2}[0-9]{2}[0-9]{2}[0-9]{2}">
                 <p class="error" id="error_tel"></p>
             </div>
             <div class="prof">
                 <label for="">Votre description</label>
                 <input type="text" id="profile" name="profile">
                 <span class="error" id="error_profile"></span>
             </div>
             <div>
                 <label for="">Le job souhaité</label>
                 <input type="text" id="metier" name="metier">
                 <span class="error" id="error_metier"></span>
             </div>
             <div class="comps">
                 <label for="">Compétences</label>
                 <input type="text" id="competences" name="competences">
<!--                 <input type="text" id="competences_content" name="competences_content">-->
                 <span class="error" id="error_competences"></span>
                 <span class="error" ></span>
             </div>
             <div class="exp">
                 <label for="">Expériences</label>
                 <input type="text" id="experiences" name="experiences" >
<!--                 <input type="text" id="experiences_content" name="experiences-content" >-->
                 <span class="error" id="error_experiences"></span>
                 <p class="error"></p>
             </div>
             <div class="forma">
                 <label for="">Formations</label>
                 <input type="text" id="formations" name="formations" >
<!--                 <input type="text" id="formations_content" name="formations_content">-->
                 <span class="error" id="error_formations"></span>
                 <p class="error"></p>
             </div>
             <div>
                 <label for="">Loisirs</label>
                 <input type="text" id="loisirs" name="loisirs">
<!--                 <input type="text" id="loisirs_content" name="loisirs_content" >-->
                 <span class="error" id="error_loisirs"></span>
                 <p class="error"></p>
             </div>
             <div class="submit">
                 <input type="submit" name="submitted" value="soumettre" class="submit_cv">
             </div>

         </form>
    </div>
</section>

<?php

get_footer();

