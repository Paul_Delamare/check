
const form= document.querySelector('.form_register');
const nb1 = document.querySelector('.nb1');
const nb2 = document.querySelector('.nb2');

// ERROR

const calculErr= document.querySelector('.calcul');
const dateErr= document.querySelector('.date_error');
const nomErr = document.querySelector('.nom_error');
const prenomErr = document.querySelector('.prenom_error');
const emailErr = document.querySelector('.email_error');
const passErr = document.querySelector('.pas2');

form.addEventListener('submit', function (e){
    e.preventDefault();
    loginBtn.disabled=true;
    calculErr.innerText='';
    dateErr.innerText= '';
    nomErr.innerText='';
    prenomErr.innerText='';
    emailErr.innerText='';
    passErr.innerText='';
        let params= new FormData();
    params.append('action', 'register_data');
    params.append('femme', form[0].checked);
    params.append('homme', form[1].checked);
    params.append('nom', form[2].value);
    params.append('prenom', form[3].value);
    params.append('email', form[4].value);
    params.append('naissance', form[5].value);
    params.append('voiture', form[6].checked);
    params.append('no_voiture', form[7].checked);
    params.append('pass1', form[8].value);
    params.append('pass2', form[9].value);
    params.append('total', parseInt(form[10].value));
    params.append('nb1', parseInt(nb1.innerText));
    params.append('nb2', parseInt(nb2.innerText));

    fetch(MYSCRIPT.ajaxUrl,{
        method: 'post',
        body: params
    }).then(function (response){
        return response.json()
    }).then(function (data){
        console.log(data);
        if (data.success!==true){
            if (data.error.length !==0 ){
                if (data.error.nom !==undefined){
                    nomErr.innerText=data.error.nom;
                }
                if (data.error.prenom !==undefined){
                    prenomErr.innerText=data.error.prenom;
                }
                if (data.error.email !==undefined){
                    emailErr.innerText=data.error.email;
                }
                if (data.error.calcul !==undefined){
                    calculErr.innerText=data.error.calcul;
                }
                if (data.error.date !==undefined){
                    dateErr.innerText= data.error.date;
                }
                if (data.error.password !==undefined){
                    passErr.innerText= data.error.password;
                }
                loginBtn.disabled=false;
            }
        }else{
            document.location.href= MYSCRIPT.home;
        }
    })
})