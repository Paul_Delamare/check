const allCv=document.querySelector('.all_list');

let params= new FormData();
params.append('action', 'get_allcv');

fetch(MYSCRIPT.ajaxUrl,{
    method: 'post',
    body: params
}).then(function (response){
    return response.json();
}).then(function (data){
    console.log(data);
    for (let i=0; i<data.cv.length; i++){
        const li = document.createElement('li');
        li.classList.add('one_cv');
        const div =document.createElement('div');
        div.classList.add('first')
        const div2 =document.createElement('div');
        div2.classList.add('second');
        const p=document.createElement('p');
        p.classList.add('name_cv');
        const p2=document.createElement('p');
        p2.classList.add('job');
        const p3=document.createElement('p');
        p3.classList.add('date_cv');
        // BLOC 1
        p.innerText=data.cv[i].name;
        p2.innerText=data.cv[i].demande;
        p3.innerText=data.cv[i].Created_at;
        div.appendChild(p);
        div.appendChild(p2);
        div.appendChild(p3);
        li.appendChild(div);

    //     BLOC 2
        const a = document.createElement('a');
        a.href='';
        a.classList.add('consulte');
        a.innerText='Consulter ce cv'
        const aDownload= document.createElement('a');
        aDownload.href='';
        aDownload.classList.add('download');
        aDownload.innerText='Télécharger ce cv'
        div2.appendChild(a);
        div2.appendChild(aDownload);
        li.appendChild(div2);
        allCv.append(li);
    }
})
