async function init () {
    const node = document.querySelector("#type-text")

    await sleep(1000)
    node.innerText = ""
    await node.type('Envie de devenir ')

    while (true) {
        await node.type('Développeur ?')
        await sleep(2000)
        await node.delete('Développeur ?')
        await node.type('Designer ?')
        await sleep(2000)
        await node.delete('Designer ?')
        await node.type('Web marketeur ?')
        await sleep(2000)
        await node.delete('Web marketeur ?')
    }
}


// Source code 🚩

const sleep = time => new Promise(resolve => setTimeout(resolve, time))

class TypeAsync extends HTMLSpanElement {
    get typeInterval () {
        const randomMs = 100 * Math.random()
        return randomMs < 50 ? 10 : randomMs
    }

    async type (text) {
        for (let character of text) {
            this.innerText += character
            await sleep(this.typeInterval)
        }
    }

    async delete (text) {
        for (let character of text) {
            this.innerText = this.innerText.slice(0, this.innerText.length -1)
            await sleep(this.typeInterval)
        }
    }
}

customElements.define('type-async', TypeAsync, { extends: 'span' })


init()


//SLIDER

$(window).on('load',function() {
    $('.flexslider').flexslider({
        animation: "slide",
        animationLoop: true,
        itemWidth: 210,
        itemMargin: 5,
        minItems: 2,
        maxItems: 4,
        controlNav: false,
        slideshowSpeed: 5000,
    });
});