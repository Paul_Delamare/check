const formCv = document.querySelector('#form-cv');


//nom
const inputNom = document.querySelector('#nom');
const errorNom = document.querySelector('#error_nom');
//metier
const inputMetier = document.querySelector('#metier');
const errorMetier = document.querySelector('#error_metier');
//profile
const inputProfile = document.querySelector('#profile');
const errorProfile = document.querySelector('#error_profile');
//email
const inputEmail = document.querySelector('.email_cv');
const errorEmail = document.querySelector('#error_email');

//Tel
const inputTel = document.querySelector('#tel');
const errorTel = document.querySelector('#error_tel');
//competence
const inputCompetences = document.querySelector('#competences');
const errorCompetences = document.querySelector('#error_competences');
// const inputCompetencesContent = document.querySelector('#competences_content');
//experiences
const inputExperiences = document.querySelector('#experiences');
const errorExperiences = document.querySelector('#error_experiences');
// const inputExperiencesContent = document.querySelector('#experiences_content');
//formations
const inputFormations = document.querySelector('#formations');
const errorFormations = document.querySelector('#error_formations');
// const inputFormationsContent = document.querySelector('#formations_content');
//loisir
const inputLoisirs = document.querySelector('#loisirs');
const errorLoisirs = document.querySelector('#error_loisirs');
// const inputLoisirsContent = document.querySelector('#loisirs_content');
const submitCv = document.querySelector('input[type=submit]');


formCv.addEventListener('submit',(e)=>{
    e.preventDefault();
    console.log({inputEmail});

    submitCv.disabled =true;
    errorNom.innerHTML =''
    errorMetier.innerHTML =''
    errorProfile.innerHTML =''
    errorEmail.innerHTML =''
    errorCompetences.innerHTML =''
    errorExperiences.innerHTML =''
    errorFormations.innerHTML =''
    errorLoisirs.innerHTML =''

    let params = new FormData();
    params.append('action', 'get_cv_data')
    params.append('nom', inputNom.value)
    params.append('metier', inputMetier.value)
    params.append('profile', inputProfile.value)
    params.append('email', inputEmail.value)
    params.append('tel', inputTel.value);
    params.append('competences', inputCompetences.value)
    // params.append('competences_content', inputCompetencesContent.value)
    params.append('experiences', inputExperiences.value)
    // params.append('experiences_content', inputExperiencesContent.value)
    params.append('formations', inputFormations.value)
    // params.append('formations_content', inputFormationsContent.value)
    params.append('loisirs', inputLoisirs.value)
    // params.append('loisirs_content', inputLoisirsContent.value)

    fetch(MYSCRIPT.ajaxUrl,{
        method:'post',
        body:params
    })
    .then(function (response){
    return response.json()
    })
    .then(function (data){
        submitCv.disabled = false;
        console.log(data);
        if (data.success){
            window.location.href = MYSCRIPT.candidat
        }else {
            if (data.errors.nom !=null ){
                errorNom.innerHTML = data.errors.nom
            }
            if (data.errors.metier !=null ){
                errorMetier.innerHTML = data.errors.metier
            }
            if (data.errors.profile !=null ){
                errorProfile.innerHTML = data.errors.profile
            }
            if (data.errors.email !=null ){
                errorEmail.innerHTML = data.errors.email
            }
            if (data.errors.tel !=null ){
                errorTel.innerHTML = data.errors.tel
            }
            if (data.errors.competence !=null ){
                errorCompetences.innerHTML = data.errors.competence;
            }
            if (data.errors.experience !=null ){
                errorExperiences.innerHTML = data.errors.experience;
            }
            if (data.errors.formation !=null ){
                errorFormations.innerHTML = data.errors.formation;
            }
            if (data.errors.loisir !=null ){
                errorLoisirs.innerHTML = data.errors.loisir
            }
        }
    })
})
