
let params = new FormData();
params.append('action', 'get_getCvAjax_data')

fetch(MYSCRIPT.ajaxUrl, {
    method: 'post',
    body: params,
}).then(function(response) {
    return response.json()
}).then(function(data) {
    //console.log(data);
    const cv_candidat = document.querySelector('.historique');
    if (data.length===0){
        const vide= document.createElement('h3');
        vide.classList.add('vide');
        vide.innerText='Aucun C.V. n\'a encore été enregistré';
        cv_candidat.append(vide);
    }else {
        data.forEach((item) => {
            const li= document.createElement('li');
            const div=document.createElement('div');
            div.classList.add('name');
            const div2=document.createElement('div');
            div2.classList.add('infos');

            const p1=document.createElement('p');
            const p2=document.createElement('p');

            const consulter= document.createElement('a');
            consulter.href='';
            consulter.classList.add('consulte_histo');
            const modifier=document.createElement('a');
            modifier.href='';
            modifier.classList.add('change');
            consulter.innerText='Voir votre C.V.';
            modifier.innerText='Modifier votre C.V.';
            p1.innerText= item["name"];
            p2.innerText=item["Created_at"];
            div2.appendChild(consulter);
            div2.appendChild(modifier);
            div.appendChild(p1);
            div.appendChild(p2);
            li.appendChild(div);
            li.appendChild(div2);
            cv_candidat.append(li);
        })
    }

});