console.log('salut');

const formLogin= document.querySelector('.connexion');
const errorLogin= document.querySelector('.error_login');
const loginBtn= document.querySelector('.submit');

formLogin.addEventListener('submit', function (e){
    e.preventDefault();
    errorLogin.innerText= '';
    loginBtn.disabled=true;
    // console.log(formLogin[0].value)
    // console.log(formLogin[1].value)
    // console.log(formLogin[2].checked)
    let params= new FormData();
    params.append('action', 'get_login_data');
    params.append('pseudo', formLogin[0].value);
    params.append('password', formLogin[1].value);
    params.append('checkbox', formLogin[2].checked);

    fetch(MYSCRIPT.ajaxUrl,{
        method: 'post',
        body: params
    }).then(function (response){
        return response.json()
    }).then(function (data){
        console.log(data);
        if (data.success!==true){
            if (data.error.length !==0 ){
                errorLogin.innerText='Le nom d\'utilisateur ou le mot de passe est incorrect*';
                loginBtn.disabled=false;
            }
        }else{
            document.location.href=MYSCRIPT.home;
        }
    })
})