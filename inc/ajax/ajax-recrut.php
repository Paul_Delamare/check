<?php

add_action('wp_ajax_get_allcv', 'getCV');
add_action('wp_ajax_nopriv_get_allcv', 'getCV');

function getCV(){
    global $wpdb;
    $cv = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}cv WHERE 1 ORDER BY Created_at DESC");
    $data=[
        'cv'=>$cv
    ];
    showJson($data);
}