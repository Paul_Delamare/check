<?php


add_action('wp_ajax_get_cv_data', 'cvAjax');
add_action('wp_ajax_nopriv_get_cv_data', 'cvAjax');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
function cvAjax(){
    global $wpdb;
//    debug($_POST);
    $id= get_current_user_id();

    $errors=[];
    $succes=false;

    $nom = cleanXss('nom');
    $metier = cleanXss('metier');
    $profile = cleanXss('profile');
    $email = cleanXss('email');
    $tel=cleanXss('tel');
    $competences = cleanXss('competences');
//    $competencesContent = cleanXss('competences_content');
    $experiences = cleanXss('experiences');
//    $experiencesContent = cleanXss('experiences_content');
    $formation = cleanXss('formations');
//    $formationContent = cleanXss('formations_content');
    $loisirs = cleanXss('loisirs');
//    $loisirsContent = cleanXss('loisirs_content');

    $errors = validationText($errors,$nom,'nom',2,255);
    $errors = validationText($errors,$metier,'metier',2,500);
    $errors = validationText($errors,$profile,'profile',2,10000);
    $errors = validationEmail($errors, $email);
    $errors=validationText($errors, $tel, 'tel',9, 11 );
    $errors = validationText($errors,$competences,'competence',2,10000);
//    $errors = validationText($errors,$competencesContent,'competence',2,10000);
    $errors = validationText($errors,$experiences,'experience',2,10000);
//    $errors = validationText($errors,$experiencesContent,'experience',2,10000);
    $errors= validationText($errors, $formation, 'formation', 2, 10000);
//    $errors= validationText($errors, $formationContent, 'formation', 2, 10000);
    $errors = validationText($errors,$loisirs,'loisir',2,10000);
//    $errors = validationText($errors,$loisirsContent,'loisir',2,10000);


    if (count($errors)===0){

        $wpdb->insert(
            $wpdb->prefix . 'cv',
            array(
                'ID_user'=>$id,
                'name' => $nom,
                'demande' => $metier,
                'email'=>$email,
                'telephone'=>$tel,
                'Skill_title'=>$competences,
                'Distinction_title'=>$profile,
                'Created_at'=>current_time('mysql'),

            ),
            array(
                '%s',
                '%s',
            )
        );
        $lastinsert = $wpdb->insert_id; //declare the last inset:last added and gice $wpbd-> insert_is
// AUTRE TABLES
        $wpdb->insert(
            $wpdb->prefix .'hobbies',
            array(
                'Id_CV' => $lastinsert,
//                'Content' => $loisirsContent,
                'Content' => $loisirs,
                'Date' => current_time('mysql'),
                'Created_at'=> current_time('mysql'),
                'Modified_at'=> current_time('mysql'),
            ),
            array(
                '%d',
                '%s',
                '%s',
                '%s',
                '%s',
            )
        );
        $wpdb->insert(
            $wpdb->prefix .'experiences',
            array(
                'ID_CV' => $lastinsert,
//                'Content' => $competencesContent,
                'Content' => $experiences,
                'Date' => current_time('mysql'),
                'Created_at'=> current_time('mysql'),
                'Modified_at'=> current_time('mysql'),
            ),
            array(
                '%d',
                '%s',
                '%s',
                '%s',
                '%s',
            )
        );
        $wpdb->insert(
            $wpdb->prefix .'diplome',
            array(
                'Id_cv' => $lastinsert,
//                'content' => $formationContent,
                'content'=> $formation,
                'date' => current_time('mysql'),
                'Created_at'=> current_time('mysql'),
                'modified_at'=> current_time('mysql'),
            ),
            array(
                '%d',
                '%s',
                '%s',
                '%s',
                '%s',
            )
        );

        $mail = new PHPMailer(true);
        try {

            $mail->isSMTP();
            $mail->Host = 'localhost';
            $mail->SMTPAuth = false;
            $mail->Port = 1025;
            $mail->CharSet='UTF-8';

            $mail->setFrom("cvcheck@gmail.com");
            $mail->addAddress("$email");

            $mail->isHTML(true);
            $mail->Subject = 'C.V. envoyé !';
            $mail->Body = get_current_user().' ton cv a bien été enregisté. Rest à l\'affut en cas de proposition de job !';

            $mail->send();
        } catch (Exception $e) {
            echo $e;
        }
        $succes=true;
    }
    $data=[
        'success'  =>$succes,
        'errors' => $errors
    ];
    showJson($data);
}