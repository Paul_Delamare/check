<?php

add_action('wp_ajax_register_data', 'registerAjax');
add_action('wp_ajax_nopriv_register_data', 'registerAjax');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\SMTP;

function registerAjax(){
    $success=false;
    $error=[];

    $femme=cleanXss('femme');
    $homme=cleanXss('homme');
    $nom=cleanXss('nom');
    $prenom=cleanXss('prenom');
    $email=cleanXss('email');
    $naissance=cleanXss('naissance');
    $voiture=cleanXss('voiture');
    $no_voiture=cleanXss('no_voiture');
    $pass1=cleanXss('pass1');
    $pass2=cleanXss('pass2');
    $total=cleanXss('total');
    $nb1=cleanXss('nb1');
    $nb2=cleanXss('nb2');

    if ($nb1+$nb2== $total){
//        add_role('candidat', 'candidat', array('read'=>true));
        $genre='';
        if ($femme=='true'){
            $genre= 'Femme';
        }elseif ($homme=='true'){
            $genre='Homme';
        }
        $error= validationText($error, $nom, 'nom', 3, 50);
        $error= validationText($error, $prenom, 'prenom', 3, 50);
        $error= validationEmail($error, $email);
        if (empty($error['email'])){
            if (email_exists($email)){
                $error['email']='Cette adresse mail existe déjà*';
            }
        }
//        DATE VERIF INFERIEUR A DATE DU JOUR
        $jour= strtotime(date('Y-m-d'));
        $date= strtotime($naissance);
        if (!empty($naissance)){
            if ($jour<$date){
                $error['date']='Date invalide*';
            }
        }else{
            $error['date']='Veuillez renseigner une date*';
        }

        if ($voiture=='true'){
            $vehicule= 'véhiculé';
        }elseif ($no_voiture=='true'){
            $vehicule= 'non véhiculé';
        }
        $error=verifAllPassword($error, $pass1, $pass2, 5);

        if (count($error)===0){
            $userdata=[
                'user_pass'=>$pass1,
                'user_login'=>$nom.$prenom,
                'user_email'=>$email,
                'first_name'=>$prenom,
                'last_name'=>$nom,
                'role'=>'candidat'
            ];

            $user_id = wp_insert_user( $userdata );
            if ( ! is_wp_error( $user_id ) ) {
                add_user_meta($user_id, 'genre', $genre, true);
                add_user_meta($user_id, 'naissance', $naissance, true);
                add_user_meta($user_id, 'véhiculé', $vehicule, true);

                wp_mail($email, 'Création de votre compte CV-Check !', 'Bonjour '.$prenom. ', nous avons le plaisir de vous informer que votre compte CV-Check a bien été crée. Reste bien à l\'affut d\'une potentiel offre d\'emploi !');

                $creds = array(
                    'user_login'    => $email,
                    'user_password' => $pass1,
                );
                wp_signon( $creds, false );



                $mail = new PHPMailer(true);
                try {

                    $mail->isSMTP();
                    $mail->Host = 'localhost';
                    $mail->SMTPAuth = false;
                    $mail->Port = 1025;
                    $mail->CharSet='UTF-8';

                    $mail->setFrom("cvcheck@gmail.com");
                    $mail->addAddress("$email");

                    $mail->isHTML(true);
                    $mail->Subject = 'Création compte CV-Check';
                    $mail->Body = "Félicitation $prenom votre compte à été crée avec succès ! Restes bien à l'affut en cas de proposition pour un job !";

                    $mail->send();
                } catch (Exception $e) {
                    echo $e;
                }
            }
            $success=true;
        }
    }else{
        $error['calcul']='Le résultat n\'est pas correct*';
    }
    $data=[
        'error'=>$error,
        'success'=>$success,
    ];
    showJson($data);
}
