<?php

add_action('wp_ajax_get_login_data', 'getLoginAjax');
add_action('wp_ajax_nopriv_get_login_data', 'getLoginAjax');

function getLoginAjax(){
    $success=false;
    $error=[];
    $pseudo=cleanXss('pseudo');
    $password=cleanXss('password');
    $checkbox=cleanXss('checkbox');

    $error=validationText($error, $pseudo, 'pseudo', 3, 100);
    $error=validationText($error, $password, 'password', 3, 100 );

    if (count($error)===0){
        $creds = array(
            'user_login'    => $pseudo,
            'user_password' => $password,
            'remember'      => $checkbox
        );
        $user = wp_signon( $creds, false );

        if ( is_wp_error( $user ) ) {
            $error['incorrect']= $user->get_error_message();
        }else{
            $success=true;
        }
    }
    $data=array(
        'error'=>$error,
        'success'=>$success
    );
    showJson($data);
}