<?php
/**
 * check functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package check
 */

if ( ! defined( '_S_VERSION' ) ) {
    define( '_S_VERSION', '1.0.0' );
}
function check_setup() {

    load_theme_textdomain( 'check', get_template_directory() . '/languages' );

    add_theme_support( 'automatic-feed-links' );
    add_theme_support( 'title-tag' );
    add_theme_support( 'post-thumbnails' );

    register_nav_menus(
        array(
            'menu-1' => esc_html__( 'Primary', 'check' ),
        )
    );
    add_theme_support(
        'html5',
        array(
            'search-form',
            'comment-form',
            'comment-list',
            'gallery',
            'caption',
            'style',
            'script',
        )
    );

    add_theme_support(
        'custom-background',
        apply_filters(
            'check_custom_background_args',
            array(
                'default-color' => 'ffffff',
                'default-image' => '',
            )
        )
    );

    add_theme_support( 'customize-selective-refresh-widgets' );

    add_theme_support(
        'custom-logo',
        array(
            'height'      => 250,
            'width'       => 250,
            'flex-width'  => true,
            'flex-height' => true,
        )
    );
}
add_action( 'after_setup_theme', 'check_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function check_content_width() {
    $GLOBALS['content_width'] = apply_filters( 'check_content_width', 640 );
}
add_action( 'after_setup_theme', 'check_content_width', 0 );

function check_widgets_init() {
    register_sidebar(
        array(
            'name'          => esc_html__( 'Sidebar', 'check' ),
            'id'            => 'sidebar-1',
            'description'   => esc_html__( 'Add widgets here.', 'check' ),
            'before_widget' => '<section id="%1$s" class="widget %2$s">',
            'after_widget'  => '</section>',
            'before_title'  => '<h2 class="widget-title">',
            'after_title'   => '</h2>',
        )
    );
}
add_action( 'widgets_init', 'check_widgets_init' );

function check_scripts() {
    wp_enqueue_style( 'check-style', get_stylesheet_uri(), array(), _S_VERSION );
    wp_style_add_data( 'check-style', 'rtl', 'replace' );
    wp_enqueue_style('font','https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,700;1,500;1,700&family=Roboto&display=swap', array(), _S_VERSION);
    wp_enqueue_style('font-awesome', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.1/css/all.min.css', array(), _S_VERSION);

    wp_deregister_script('jquery');
    wp_enqueue_script('jquery','https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.3/jquery.min.js', array(), _S_VERSION, true);

    wp_enqueue_script('login-js', get_template_directory_uri().'/asset/js/login.js', array(),_S_VERSION, true );
    wp_enqueue_script('burger-js', get_template_directory_uri().'/asset/js/burger.js', array(),_S_VERSION, true );


    wp_localize_script('login-js','MYSCRIPT', array(
        'ajaxUrl'=>admin_url('admin-ajax.php'),
        'home'   => path('/'),
        'candidat'   => path('candidat'),

    ));

    wp_add_inline_script('jquery', 'const MYSCRIPT = ' . json_encode(array(
            'ajaxUrl' => admin_url('admin-ajax.php'),
            'home' => path('/'),
            'candidat'   => path('candidat'),
            'theme' => get_template_directory_uri(),
        )), 'before');

    if (is_page_template('template-home.php')){
        wp_enqueue_style('flexslider-style',get_template_directory_uri().'/asset/flexslider/flexslider.css', array(), _S_VERSION);
        wp_enqueue_style('map-style','https://cdnjs.cloudflare.com/ajax/libs/leaflet/1.9.3/leaflet.css', array(), _S_VERSION);
        wp_enqueue_script('leaftlet-js', 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/1.9.3/leaflet.js', array(),_S_VERSION);
        wp_enqueue_script('home-js', get_template_directory_uri().'/asset/js/home.js', array(),_S_VERSION, true );
        wp_enqueue_script('flexslider-js', get_template_directory_uri().'/asset/flexslider/jquery.flexslider-min.js', array(),_S_VERSION, true );
    }

    if (is_page_template('template-register.php')){
        wp_enqueue_script('register-js', get_template_directory_uri().'/asset/js/register.js', array(),_S_VERSION, true );
        wp_localize_script('register-js','MYSCRIPT', array(
            'ajaxUrl'=>admin_url('admin-ajax.php'),
            'candidat'   => path('candidat'),
            'home'   => path('/')
        ));
    }


    if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
        wp_enqueue_script( 'comment-reply' );
    }
    // cv
    if (is_page_template('template-cv.php')){
        wp_enqueue_script('cv', get_template_directory_uri().'/asset/js/cv.js', array(), _S_VERSION, true);
        wp_localize_script('cv', 'MYSCRIPT',array(
            'ajaxUrl'=> admin_url('admin-ajax.php'),
            //cree la redirection apres la connexion
            'home' => path('/'),
            'candidat'   => path('candidat'),

        ));
    }
    if(is_page_template( 'template-recrut.php', )) {
        wp_enqueue_script('recrutjs', get_template_directory_uri() . '/asset/js/recrut.js', array(), _S_VERSION, true);
    }

    if (is_page_template('template-candidat.php')) {
        wp_enqueue_script('candidat-js', get_template_directory_uri() . '/asset/js/candidat.js', array(), _S_VERSION, true);
        wp_localize_script('candidat-js', 'MYSCRIPT', array(
            'ajaxUrl' => admin_url('admin-ajax.php')
        ));
    }
    if (is_page_template('template-historique.php')){
        wp_enqueue_style('historique.js', get_template_directory_uri().'/asset/js/histo.js', array(), _S_VERSION);
    }


}

add_action( 'wp_enqueue_scripts', 'check_scripts' );

