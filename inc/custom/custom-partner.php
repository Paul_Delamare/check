<?php
/**
 * Register a custom post type called "partner".
 *
 * @see get_post_type_labels() for label keys.
 */
function wpdocs_codex_partner_init() {
    $labels = array(
        'name'                  => _x( 'partners', 'Post type general name', 'wonder' ),
        'singular_name'         => _x( 'partner', 'Post type singular name', 'wonder' ),
        'menu_name'             => _x( 'partners', 'Admin Menu text', 'wonder' ),
        'name_admin_bar'        => _x( 'partner', 'Add New on Toolbar', 'wonder' ),
        'add_new'               => __( 'Ajouter', 'wonder' ),
        'add_new_item'          => __( 'Ajouter un nouveau partner', 'wonder' ),
        'new_item'              => __( 'Nouveau partner', 'wonder' ),
        'edit_item'             => __( 'Modifier partner', 'wonder' ),
        'view_item'             => __( 'Voir partner', 'wonder' ),
        'all_items'             => __( 'Tous les partners', 'wonder' ),
        'search_items'          => __( 'Rechercher partner', 'wonder' ),
        'parent_item_colon'     => __( 'Parent partner:', 'wonder' ),
        'not_found'             => __( 'Aucun partner n\'est trouvé.', 'wonder' ),
        'not_found_in_trash'    => __( 'Aucun partner n\'est trouvé dans Trash.', 'wonder' ),
        'featured_image'        => _x( 'partner Cover Image', 'Overrides the “Featured Image” phrase for this post type. Added in 4.3', 'wonder' ),
        'set_featured_image'    => _x( 'Set cover image', 'Overrides the “Set featured image” phrase for this post type. Added in 4.3', 'wonder' ),
        'remove_featured_image' => _x( 'Remove cover image', 'Overrides the “Remove featured image” phrase for this post type. Added in 4.3', 'wonder' ),
        'use_featured_image'    => _x( 'Use as cover image', 'Overrides the “Use as featured image” phrase for this post type. Added in 4.3', 'wonder' ),
        'archives'              => _x( 'partner archives', 'The post type archive label used in nav menus. Default “Post Archives”. Added in 4.4', 'wonder' ),
        'insert_into_item'      => _x( 'Insert into partner', 'Overrides the “Insert into post”/”Insert into page” phrase (used when inserting media into a post). Added in 4.4', 'wonder' ),
        'uploaded_to_this_item' => _x( 'Uploaded to this partner', 'Overrides the “Uploaded to this post”/”Uploaded to this page” phrase (used when viewing media attached to a post). Added in 4.4', 'wonder' ),
        'filter_items_list'     => _x( 'Filter partners list', 'Screen reader text for the filter links heading on the post type listing screen. Default “Filter posts list”/”Filter pages list”. Added in 4.4', 'wonder' ),
        'items_list_navigation' => _x( 'partners list navigation', 'Screen reader text for the pagination heading on the post type listing screen. Default “Posts list navigation”/”Pages list navigation”. Added in 4.4', 'wonder' ),
        'items_list'            => _x( 'partners list', 'Screen reader text for the items list heading on the post type listing screen. Default “Posts list”/”Pages list”. Added in 4.4', 'wonder' ),
    );

    $args = array(
        'labels'             => $labels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'partner' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'menu_icon'          => 'dashicons-admin-users',
        'supports'           => array( 'title', 'editor', 'thumbnail', 'excerpt'),
    );

    register_post_type( 'partner', $args );
}

add_action( 'init', 'wpdocs_codex_partner_init' );