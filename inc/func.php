<?php

function debug($var,$height = 200,$fixed = false)
{
    $backt = debug_backtrace()[0];
    if($fixed) {
        echo '<pre style="position: fixed;top:0;left:0;right:0;height:'.$height.'px;z-index:999999;overflow-y: scroll;font-size:.8em;padding: 10px 10px 10px 220px; font-family: Consolas, Monospace; background-color: #000; color: #fff;">';
    } else {
        echo '<pre style="height:'.$height.'px;z-index:999999;overflow-y: scroll;font-size:.8em;padding: 10px 10px 10px 10px; font-family: Consolas, Monospace; background-color: #000; color: #fff;">';
    }
    echo '<p style="font-size:.85rem;">'.$backt['file'].' - '.$backt['line'].'</p>';
    print_r($var);
    echo '</pre>';
}

function path($a){
    return esc_url(home_url($a));
}

function asset($link){
    return get_template_directory_uri().'/asset/'.$link;
}
function getImg($id, $size, $class=null){
    if (!empty($class)){
        return '<img class="'.$class.'" src="'.get_the_post_thumbnail_url($id, $size).'"> ';
    }else{
        return '<img src="'.get_the_post_thumbnail_url($id, $size).'"> ';
    }
}
function imgById($meta, $id, $format=null)
{
    $img = wp_get_attachment_image_src($meta[$id][0], $format);
    if (!empty($img)) {
        return '<img src="' . $img[0] . '">';
    }
}

function getTextByKey($metas, $key, $class=null){
    if (!empty($metas[$key][0])){
        if (!empty($class)) {
            return '<p class="'.$class.'">' . $metas[$key][0] . '</p>';
        }else{
            return '<p>' . $metas[$key][0] . '</p>';
        }
    }else{
        return'';
    }
}

function cleanXss($key){
    return trim(strip_tags($_POST[$key]));
}

function validationText($errors, $data, $keyError, $min, $max){
    if (!empty($data)){
        if (mb_strlen($data) < $min){
            $errors[$keyError]='Veuillez renseignez plus de '.$min.' caractères*';
        }elseif (mb_strlen($data)> $max){
            $errors[$keyError]='Veuillez renseignez moins de '.$max.' caractères*';
        }
    }else{
        $errors[$keyError]='Veuillez renseignez ce formulaire*';
    }
    return $errors;
}
function viewError($errors,$key)
{
    if(!empty($errors[$key])) {
        return $errors[$key];
    }
}

function getPostValue($key, $data = '')
{
    if(!empty($_POST[$key]) ) {
        echo $_POST[$key];
    } elseif(!empty($data)) {
        echo $data;
    }
}
function showJson($data){
    header("content-type: application/json");
    $json = json_encode($data, JSON_PRETTY_PRINT);
    if ($json){
        die($json);
    }else{
        die('error in json encoding');
    }
}
function validationEmail($errors, $mail1, $key='email' ){
    if (!empty($mail1)){
        if (!filter_var($mail1, FILTER_VALIDATE_EMAIL)){
            $errors[$key]='Veuillez renseignez un email valide*';
        }
    }else{
        $errors[$key]= 'Veuillez rentrer une adresse email*';
    }
    return $errors;
}
function verifAllPassword($error, $password, $password2, $min){
    if(!empty($password) && !empty($password2)) {
        if($password != $password2) {
            $error['password'] = 'Vos mots de passe sont différents*';
        } elseif(mb_strlen($password) < $min) {
            $error['password'] = 'Votre mot de passe est trop court(min '.$min.')';
        }
    } else {
        $error['password'] = 'Veuillez renseigner les mots de passe*';
    }
    return $error;
}

function getMetaText($metas, $key){
    if (!empty($metas[$key][0])) {
        return $metas[$key][0];
    }
}

function current_user_is($role){
    $current_user = wp_get_current_user();
    if($current_user->data->ID != 0){
        if(in_array($role, $current_user->roles)){
            return true;
        }
        else {
            return false;
        }
    }
    else{
        return false;
    }
}
function wpm_admin_redirection() {
    global $web;

//    if ( is_admin() && current_user_can( 'candidat' ) ) {
//        wp_redirect( path($web['page']['homepage']['slug']) );
//        exit;
//    }
}
add_action( 'init', 'wpm_admin_redirection' );
function getImage($imge,$keyIm){
    if (!empty($imge[$keyIm][0])){
        echo $imge[$keyIm][0];
        $img = wp_get_attachment_image_src($imge[$keyIm][0],'img_work');
        debug($img);
        echo '<img src="'.$img[0].'" alt=""/>';
    }else{
        return ' ';
    }
}
